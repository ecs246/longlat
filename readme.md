
# Lat Lng Project

## Requirements
    docker
    docker-compose


##Setting it up
Git clone this repo. 

    git clone git@bitbucket.org:ecs246/longlat.git

Go into the directory. longlat

Start the local docker image:

    docker-compose up

The errors and notices should pop up in this terminal.

Open a new terminal and go back into that directory longlat.

Setup the database:

    docker-compose exec app php bin/console doctrine:database:create

    docker-compose exec app php bin/console doctrine:query:sql  "CREATE EXTENSION postgis"

Run database migrations:

    docker-compose exec app php bin/console doctrine:migrations:migrate

Run database populating script:
    
    docker-compose exec app php bin/console app:load-db


Local should now be accessible at: [http://localhost:8999/posts/proximity]

##Routes
http://localhost:8999/posts/proximity

    - This route curls to the resources 
        https://jsonplaceholder.typicode.com/posts, https://jsonplaceholder.typicode.com/users 
        to generate data. 

    - Everytime you hit this route it will curl to those resources. 

    - The request payload via post should be. I use postman. https://www.getpostman.com/
        {
        "geo": {
            "lat": "-43.1234",
            "lng": "81.1496"
        }
        }

    - It uses the https://github.com/jsor/geokit/tree/v1.3.0 library to calculate distance between points.

http://localhost:8999/posts/proximity2

    - This route uses cached data in postgres with postgis extension turned on. 

    - It uses the postgis spatial index to calculate distances between points https://postgis.net/

##Notes

This is a symfony 4 project that uses auto wiring. 

Things to note if you are unfamiliar with symfony. 

The main src is here 
    https://bitbucket.org/ecs246/longlat/src/master/src/

The controller logic             
    https://bitbucket.org/ecs246/longlat/src/master/src/Controller/ProximityController.php

The db migrate logic 
    https://bitbucket.org/ecs246/longlat/src/master/src/Command/LoadDbCommand.php


