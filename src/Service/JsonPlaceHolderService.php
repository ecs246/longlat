<?php
namespace App\Service;

class JsonPlaceHolderService {
    private $postsRoute = "https://jsonplaceholder.typicode.com/posts";
    private $usersRoute = "https://jsonplaceholder.typicode.com/users";

    public function getPosts(): array {
        return $this->getResource($this->postsRoute);
    }

    public function getUsers(): array {
        return $this->getResource($this->usersRoute);

    }

    public function findClosestUser($lat, $lng) {
        $position = \Geokit\LatLng::normalize(['lat' => $lat, 'lng' => $lng]);

        $users = $this->getUsers();
        $min = null;
        $minUser = null;
        $math = new \Geokit\Math();
        foreach ($users as $user) {
            $lng = $user['address']['geo']['lng'];
            $lat = $user['address']['geo']['lat'];

            $tmppos = \Geokit\LatLng::normalize(['lat' => $lat, 'lng' => $lng]);

            $distance = $math->distanceVincenty($position, $tmppos);
            $distance = $distance->feet();
            if ($minUser == null) {
                $minUser = $user;
                $min = $distance;
            } else {
                if ($min > $distance) {
                    $min = $distance; 
                    $minUser = $user;
                }
            }


        }
        return $minUser;
    } 

    public function getArticlesFromUserId($id) {
        $ret = [];
        $posts = $this->getPosts();
        foreach ($posts as $post) {
            if ($post['userId'] == $id) {
                $ret[] = $post;
            }
        }
        return $ret;
    }
    protected function getResource($url): array {
        
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url, []);
        $body = $res->getBody();
        $data = json_decode($body, true);
        if (!$data) {
            throw \Exception("Can't Parse Data");
        }
        return $data;
    }


}