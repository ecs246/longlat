<?php

namespace App\RequestConstraint;

use Symfony\Component\Validator\Constraints as Assert;


class ProximityConstraint
{    
    public function getSchema()
    {
        return [
            'geo' => new Assert\Collection([
                'lat' => $this->getPointValidation(), 
                'lng' => $this->getPointValidation()
            ])
        ];
    }

    private function getPointValidation() {
        return [
            new Assert\NotBlank(),
            new Assert\Regex([
                'pattern' => '/^-?\d*\.?\d*$/'
                ])
        ];
    }   
}
