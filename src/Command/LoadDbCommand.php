<?php
namespace App\Command;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\JsonPlaceHolderService;
use App\Entity\User;
use App\Entity\Post;

class LoadDbCommand extends Command
{
    protected static $defaultName = 'app:load-db';
    protected $jps;
    protected $em;
    protected function configure()
    {
        $this
            ->setDescription('Load db')
            ->setHelp('Loads a Db')
        ;
    }
    public function __construct(string $name = null,  JsonPlaceHolderService $jps, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->jps = $jps;
        $this->em = $em;
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $posts = $this->jps->getPosts();
        $users = $this->jps->getUsers();
        $output->writeln([
            "Inserting users"
        ]);

        $this->batchInsert($users, function($item) {
            $repo = $this->em->getRepository(User::class);
            $user = $repo->findOneBy(['id' =>$item['id']]);
            if (!$user) {
                $user = new User();
            }
            $user->setId($item['id']);
            $user->setName($item['name']);
            $user->setEmail($item['email']);
            $lng = $item['address']['geo']['lng'];
            $lat = $item['address']['geo']['lat'];
            $user->setCoordinates("POINT({$lng} {$lat})");
            return $user;
        });

        $output->writeln([
            "Inserting Posts"
        ]);

        $this->batchInsert($posts, function($item) {
            $repo = $this->em->getRepository(Post::class);
            $post = $repo->findOneBy(['id' =>$item['id']]);
            if (!$post) {
                $post = new Post();
            }
            $post->setId($item['id']);
            $post->setUserId($item['userId']);
            $post->setTitle($item['title']);
            $post->setBody($item['body']);
            return $post;
        }); 
    }


    protected function batchInsert(array $items, $f) {
        $batchSize = 20;
        $em = $this->em;
        foreach ($items as $i => $item) {
            //$product = new Product($item['datas']);
            $entity = $f($item);
            $em->persist($entity);

            // flush everything to the database every 20 inserts
            if (($i % $batchSize) == 0) {
                $em->flush();
                $em->clear();
            }
        }

        // flush the remaining objects
        $em->flush();
        $em->clear();

    }
}