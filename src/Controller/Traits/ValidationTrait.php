<?php

namespace App\Controller\Traits;

use Symfony\Component\Validator\Constraints\Collection as ConstraintsCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\HttpFoundation\JsonResponse;

trait ValidationTrait {


    protected function validateRequest(Request $request, array $constraintSchema): array
    {
        $constraint = new ConstraintsCollection($constraintSchema);
        $data = json_decode($request->getContent(), true);

        if (!$data) {
            $r = new JsonResponse(["error" => "Malformed content"], 400);
            $r->send();
            //throw new BadRequestHttpException("Malformed content");
        }
        $validator = Validation::createValidator();
        $violations = $validator->validate($data, $constraint);

        if (count($violations) > 0) {
            $msg = [];
            foreach ($violations as $violation) {

                $msg[]  = ["Field" =>  $violation->getPropertyPath(),
                         "Violation" =>  $violation->getMessage()];
            }
            $r = new JsonResponse(["error" => $msg], 400);
            $r->send();
            //throw new BadRequestHttpException($msg);
        }
        return $data;
    }
}