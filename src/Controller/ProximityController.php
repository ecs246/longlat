<?php

namespace App\Controller;

use App\RequestConstraint\ProximityConstraint;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Controller\Traits\ValidationTrait;
use App\Service\JsonPlaceHolderService;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\PostRepository;
use App\Repository\UserRepository;

class ProximityController
{
    use ValidationTrait;

    protected $jps;
    protected $postRepo;
    protected $userRepo;

    public function __construct(
        JsonPlaceHolderService $jps,
        PostRepository $postRepo,
        UserRepository $userRepo
    )
    {
        $this->jps = $jps;
        $this->postRepo = $postRepo;
        $this->userRepo = $userRepo;
    }
    /**
     * Finds the nearest user posts based on the longitude and latitude passed in.
     */
    public function proximity(Request $request)
    {
        $constraint = new ProximityConstraint();
        $data = $this->validateRequest($request, $constraint->getSchema());
        $lat = $data['geo']['lat'];
        $lng = $data['geo']['lng'];
        $user = $this->jps->findClosestUser($lat, $lng);
        $userId = $user['id'];
        $articles = $this->jps->getArticlesFromUserId($userId);
        $response = new JsonResponse($articles);
        return $response;
    }
    /**
     * Finds the nearest user posts based on the longitude and latitude passed in.
     * Uses database as a cache.
     */
    public function proximity2(Request $request)
    {
        $constraint = new ProximityConstraint();
        $data = $this->validateRequest($request, $constraint->getSchema());
        $lat = $data['geo']['lat'];
        $lng = $data['geo']['lng'];

        $user = $this->userRepo->findClosestUser($lat, $lng);
    
        $userId = $user->getId();
        $posts = $this->postRepo->findBy(["user_id" => $user->getId()]);
        $data = [];
        foreach ($posts as $post) {
            $data[] = [
                "userId" => $post->getUserId(),
                "id" => $post->getId(),
                "title" => $post->getTitle(),
                "body" => $post->getBody(),
            ];
        }
        $response = new JsonResponse($data);
        return $response;
    }

}